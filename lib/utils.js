import {utils} from 'littlefork-core';

const {assertCfg} = utils.assertions;

export const assertKey = assertCfg(['guardian.key']);

export default { assertKey };
